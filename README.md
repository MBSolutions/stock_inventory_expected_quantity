Master

[![Pipeline status](https://gitlab.com/m9s/stock_inventory_expected_quantity/badges/master/pipeline.svg)](https://gitlab.com/m9s/stock_inventory_expected_quantity/commits/master)

Develop

[![Pipeline status](https://gitlab.com/m9s/stock_inventory_expected_quantity/badges/develop/pipeline.svg)](https://gitlab.com/m9s/stock_inventory_expected_quantity/commits/develop)

[![Coverage report](https://gitlab.com/m9s/stock_inventory_expected_quantity/badges/develop/coverage.svg)](http://m9s.gitlab.io/stock_inventory_expected_quantity)



This module runs with the Tryton application platform.

Installing
----------

See INSTALL

Note
----

This module is developed and tested over a patched Tryton server and
core modules. Maybe some of these patches are required for the module to work.

Support
-------

For more information or if you encounter any problems with this module,
please contact the programmers at

#### MBSolutions

   * Issues:   https://gitlab.com/m9s/stock_inventory_expected_quantity/issues
   * Website:  http://www.m9s.biz/
   * Email:    info@m9s.biz

If you encounter any problems with Tryton, please don't hesitate to ask
questions on the Tryton bug tracker, forum or IRC channel:

   * http://bugs.tryton.org/
   * http://www.tryton.org/forum
   * irc://irc.freenode.net/tryton

License
-------

See LICENSE

Copyright
---------

See COPYRIGHT

